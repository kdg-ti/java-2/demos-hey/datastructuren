import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        // Create tree with root node
        Node tree = new Node(1,
                new Node(2,
                        new Node(3),
                        new Node(7),
                        new Node(9)),
                new Node(12,
                        new Node(4),
                        new Node(6)));

        //printDepth(tree);

        printBreadth(tree);
    }

    public static void hanoi(Stack<Integer> from, Stack<Integer> to, Stack<Integer> aux){
        discMove(from, to, aux, from.size());
    }

    public static void discMove(Stack<Integer> from, Stack<Integer> to, Stack<Integer> aux, int discMove){
        // TODO
        // Note: discMove is for how many discs we are trying to move currently
        // Basecase is moving 1 disc
        throw new UnsupportedOperationException();
    }

    public static int GCD(int a, int b){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static int gaussSum(int n){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static int factorial(int n){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static int fibb(int n){
        // TODO
        throw new UnsupportedOperationException();
    }

    static int leaves(Node node)
    {
        // TODO
        throw new UnsupportedOperationException();
    }

    static int sum(Node node){
        // TODO
        throw new UnsupportedOperationException();
    }

    static int depth(Node node){

        // TODO
        throw new UnsupportedOperationException();
    }

    static String path(Node node, int value){
        // TODO
        throw new UnsupportedOperationException();
    }

    static void printDepth(Node node){

        // TODO
        throw new UnsupportedOperationException();

    }

    static void printBreadth(Node node){

        // TODO
        throw new UnsupportedOperationException();

    }

}
