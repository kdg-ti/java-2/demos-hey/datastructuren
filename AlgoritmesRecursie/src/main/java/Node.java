import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Node {

    private int value;

    private List<Node> children;

    Node(int value, Node... nodes)
    {
        this.value = value;

        if(nodes.length > 1)
            children = new ArrayList<>(nodes.length);

        for(Node n : nodes)
            children.add(n);
    }

    List<Node> getChildren(){
        return Collections.unmodifiableList(children);
    }

    boolean leaf(){
        return children == null;
    }

    int getValue(){
        return value;
    }

}
