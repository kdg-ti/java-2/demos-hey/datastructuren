import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Stack;

public class TestHanoi {

    @Test
    public void testHanoiOneDisc(){
        testHanoiNDisc(1);
    }

    @Test
    public void testHanoiTwoDisc(){
        testHanoiNDisc(2);
    }

    @Test
    public void testHanoiThreeDisc(){
        testHanoiNDisc(3);
    }

    @Test
    public void testHanoiTenDisc(){
        testHanoiNDisc(10);
    }

    public void testHanoiNDisc(int discs){

        // Create rods
        Stack<Integer> rod1 = new Stack<>();
        Stack<Integer> rod2 = new Stack<>();
        Stack<Integer> rod3 = new Stack<>();

        // Push requested discs
        for (int i = discs; i > 0; i--)
            rod1.push(i);

        // Run hanoi
        Main.hanoi(rod1, rod3, rod2);

        String discOrder = "";
        boolean inOrder = true;

        // Check if discs are in requested order
        for (int i = 1; i <= discs; i++) {
            int disc = rod3.pop();
            inOrder &= (i == disc);
            discOrder += disc + ", ";
        }

        discOrder = discOrder.substring(0, discOrder.length() - 2);

        Assertions.assertTrue(inOrder, "Discs are not in order: " + discOrder);
    }

}
