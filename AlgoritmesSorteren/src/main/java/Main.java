public class Main {

    public static void main(String[] args)
    {
        // [8, 7, 2, 6, 1, 5, 9, 3, 4]
        int[] nums = new int[]{8, 7, 2, 6, 1, 5, 9, 3, 4};
        sortQuick(nums);
        System.out.println(arrayToString(nums));
    }

    // Selection sort
    public static void sortSelection(int[] unsorted){
        // TODO
        throw new UnsupportedOperationException();
    }

    // Merge sort
    public static void sortMerge(int[] unsorted){

        // TODO
        throw new UnsupportedOperationException();
    }

    public static void merge(int[] sorted1, int[] sorted2)
    {
        // TODO
        throw new UnsupportedOperationException();
    }

    // Quick sort
    public static void sortQuick(int[] unsorted){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static void sortQuickRec(int[] unsorted, int low, int high){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static int partition(int[] unpart, int low, int high){

        // TODO
        throw new UnsupportedOperationException();
    }

    // Random sort
    public static void sortRandom(int[] unsorted){
        // TODO
        throw new UnsupportedOperationException();
    }

    public static String arrayToString(int[] array){
        StringBuilder builder = new StringBuilder();

        builder.append("[");
        for (int i = 0; i < array.length; i++)
        {
            builder.append(array[i]);
            builder.append(", ");
        }
        builder.setLength(builder.length() - 2);
        builder.append("]");
        return builder.toString();
    }

}
