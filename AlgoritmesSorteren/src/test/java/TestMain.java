import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

public class TestMain {

    @Test
    public void testSelection(){
        testSortingAlgo(Main::sortSelection);
    }

    @Test
    public void testMerge(){
        testSortingAlgo(Main::sortMerge);
    }

    @Test
    public void testQuickSort(){
        testSortingAlgo(Main::sortQuick);
    }

    @Test
    public void testRandom(){
        testSortingAlgo(Main::sortRandom);
    }

    public void testSortingAlgo(Consumer<int[]> f){

        // Input and output array
        int[] input;
        int[] output;
        int[] expected;

        // Empty array
        input = new int[]{};
        f.accept(input);
        output = input.clone();
        expected = new int[]{};
        Assertions.assertArrayEquals(expected, output,
                String.format("Input: %s Output: %s Expected: %s",
                        arrayToString(input),
                        arrayToString(output),
                        arrayToString(expected)));

        // Array with 1 element
        input = new int[]{ 1 };
        output = input.clone();
        f.accept(output);
        expected = new int[]{ 1 };
        Assertions.assertArrayEquals(expected, output,
                String.format("Input: %s Output: %s Expected: %s",
                        arrayToString(input),
                        arrayToString(output),
                        arrayToString(expected)));

        // Array with 2 elements
        input = new int[]{ 1, 2 };
        output = input.clone();
        f.accept(output);
        expected = new int[]{ 1, 2 };
        Assertions.assertArrayEquals(expected, output,
                String.format("Input: %s Output: %s Expected: %s",
                        arrayToString(input),
                        arrayToString(output),
                        arrayToString(expected)));

        // Array with 2 elements
        input = new int[]{ 2, 1 };
        output = input.clone();
        f.accept(output);
        expected = new int[]{ 1, 2 };
        Assertions.assertArrayEquals(expected, output,
                String.format("Input: %s Output: %s Expected: %s",
                        arrayToString(input),
                        arrayToString(output),
                        arrayToString(expected)));

        // Array with n elements
        input = new int[]{ 8, 7, 2, 6, 1, 5, 9, 3, 4 };
        output = input.clone();
        f.accept(output);
        expected = new int[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        Assertions.assertArrayEquals(expected, output,
                String.format("Input: %s Output: %s Expected: %s",
                        arrayToString(input),
                        arrayToString(output),
                        arrayToString(expected)));

    }

    public String arrayToString(int[] array){
        StringBuilder builder = new StringBuilder();

        builder.append("[");
        for (int i = 0; i < array.length; i++)
        {
            builder.append(array[i]);
            builder.append(", ");
        }
        if(array.length > 0)
            builder.setLength(builder.length() - 2);
        builder.append("]");
        return builder.toString();
    }

}
