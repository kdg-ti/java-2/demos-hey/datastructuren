import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BNode {

    private int value;

    private BNode left;

    private BNode right;

    BNode(int value){
        this.value = value;
    }

    BNode(int value, BNode left, BNode right){
        this.value = value;

        this.left = left;
        this.right = right;
    }

    public BNode getLeft(){
        return left;
    }

    public void setLeft(BNode left){
        this.left = left;
    }

    public BNode getRight(){
        return right;
    }

    public void setRight(BNode right){
        this.right = right;
    }

    boolean leaf(){
        return (left == null) && (right == null);
    }

    int getValue(){
        return value;
    }

}
